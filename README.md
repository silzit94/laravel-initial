docker-compose up -d

docker-compose exec app sh

cp .env.example .env

composer install

php artisan key:generate

\*\* Note

- Remember update database in the .env file

\*\* Access

- http://localhost/

- http://localhost:8080/ -> PHPMyAdmin
